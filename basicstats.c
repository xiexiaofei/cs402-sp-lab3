//
//  main.c
//  lab3
//
//  Created by xiaofei xie on 4/24/19.
//  Copyright © 2019 xiaofei xie. All rights reserved.
//

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>

#define ENLARGE_TIMES 2
#define ENLARGE_FACTOR 0.8

FILE* file;

// use fopen to open the file with input parameter of char* filename
// return 0 : success
// return 1 : error
int openFile(char *filename){
    file = fopen (filename, "r+");
    return file ? 0 : 1;
}

// use fscanf to get the float varaible of the input file line by line
// return 0 : success
// return 1 : error
int readFloat(float* f) {
    return fscanf(file, "%f", f) == 1 ? 0 : 1;
}

// enLarge the old Array if the size is greater thant the capacity
// step1:make ENLARGE_FACTOR * old capacity as new capacity
// step2:copy the element in old array to new array to transfer the data
// step3:free the old array space and referrence
float* enLarge(float *array, int size, int capacity){
    
    float *newArray = NULL;
    // step1 :
    int newCapacity = ENLARGE_TIMES * capacity;
    newArray = (float*)malloc(newCapacity*sizeof(float));
    
    // step2:
    int count = 0;
    while (count < size) {
        newArray[count] = array[count];
        count++;
    }
    
    // step3:
    free(array);
    array = NULL;
    
    return newArray;
}

// construct my comparator for qucikSort, which is compare float a and b
// if a == b, return 0
// if a < b, return -1
// if a > b, return 1
int myCompare(const void*a,const void*b) {
    if (*(float*)a == *(float*)b) {
        return 0;
    }
    return (*(float*)a-*(float*)b)>0 ? 1 : -1;
}


// read the file and save the data in our array using malloc funciton
// we assume the enlarge factor is 0.8, which means if the real size of array reach 80% of
// the capacity of array, we will call enlarge function to double the size
float* getFile(char* filename, int *arrayCapacity, int *arraySize) {
    int size = 0;
    int capacity = 20;
    float* array = (float*)malloc(20*sizeof(float));
    
    int fileReturn = openFile(filename);
    
    if (fileReturn != 0) {
        printf("Can not open file! %s\n", filename);
        exit(0);
    }
    
    while (100) {
        if(size>=capacity * ENLARGE_FACTOR){
            array = enLarge(array,size,capacity);
            capacity *= 2;
        }
        fileReturn = readFloat(&array[size]);
        if (fileReturn != 0) {
            break;
        }
        size = size + 1;
    }
    
    *arraySize = (int)size + 0;
    *arrayCapacity = (int)capacity + 0;
    
    int row = size + 0;
    int col = sizeof(array[0]) + 0;
    qsort((void *)array, row, col, myCompare);
    
    return array;
}

// get mean can be divided into 2 steps:
// step 1 : get sum
// step 2: get number of elements
// sum / number is our mean result
double getMean(float *array, int arraySize){
    int count = 0;
    double sum = 0.00;
    while (count < arraySize) {
        sum = array[count] + sum;
        count++;
    }
    
    return (double) sum / arraySize;
}

// there are two situation for get median
// for odd size of array : the media element is what we need
// for even size of array : the mean of preMiddle and post Middle is our result
double getMedian(float *array, int arraySize){
    double res = 0;
    if(arraySize % 2 == 1){
        return array[arraySize/2 + 0];
    }else{
        double midPre = (double)array[(int)(arraySize/2)-1];
        double midPost = (double)array[(int)(arraySize/2)];
        res = midPre + (midPost - midPre)/2;
    }
    return (double)res;
}

// for get standard deviation
// step 1: get mean using our mean()
// step 2: iterative the array to get the standard deviation
double getStddev(float *array, int arraySize){
    double meanValue = getMean(array,arraySize);
    double difference = 0;
    int count = 0;
    while (count < arraySize) {
        difference = (array[count] - meanValue) * (array[count] - meanValue) + difference;
        count++;
    }
    return (double)sqrt(difference/arraySize);
}

// main funciton :
// step1 : read the input file (small.txt OR large.txt)
// step2 : sort the data
// step3 : getMean();
// step4 : getMedian();
// step5 : get standard deviation of the set of values
int main(int argc, char** argv) {
    // for testing :
//    argc=2;
//    argv[0]="./basicstats";
////    argv[1]="/Users/xiaofeixie/Desktop/lab3/lab3/small.txt";
//    argv[1]="/Users/xiaofeixie/Desktop/lab3/lab3/large.txt";
    
    // initialize the all the variable
    char* fileName = "";
    int capacity = 0;
    int size = 0;
    
    if (argc == 2 && !strcmp(argv[0], "./basicstats")) {
        fileName = argv[1];
    }
    
    float* array = getFile(fileName,&capacity,&size);
    double median = (double)getMedian(array,size);
    double mean = (double)getMean(array,size);
    double stddev = (double)getStddev(array,size);
    
    printf("Results:\n");
    printf("*****************\n");
    printf("Num values:\t\t\t\t  %d\n",size);
    printf("Mean:\t\t\t\t\t%.3f\n",mean);
    printf("Median:\t\t\t\t\t%.3f\n",median);
    printf("Stddev:\t\t\t\t\t%.3f\n",stddev);
    printf("Unused array capacity:\t%d\n",capacity-size);
    
    return 0;
}

